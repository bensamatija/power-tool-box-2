﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

//
using System.Collections;
using System.Runtime.InteropServices;

//using Tekla.Structures.Model;
using TSM = Tekla.Structures.Model;
using TSD = Tekla.Structures.Drawing;
using T3D = Tekla.Structures.Geometry3d;
using TSMUI = Tekla.Structures.Model.UI;
using TSMI = Tekla.Structures.ModelInternal;
using TSO = Tekla.Structures.Model.Operations;
using TSI = Tekla.Structures.Internal;
using TMB = TeklaMacroBuilder;
//using System.Collections;
//using Tekla.Technology.MacroSelector;
//using Tekla.Technology.Akit;
//using Tekla.Macros;
//using Tekla.Macros.Runtime;
//using Tekla.Technology.Scripting;


namespace PowerToolBox2
{
    class CodeBase
    {

        public TSM.Model GetModelConnectionStatus()
        {
            TSM.Model myModel = new TSM.Model();

            if (!myModel.GetConnectionStatus())
            {
                //MessageBox.Show("Tekla is not open.", Variables.appName + Variables.appVersion);
                //Environment.Exit(1);
            }
            return myModel;
        }

        public bool ModelConnectionExists()
        {
            TSM.Model myModel = new TSM.Model();

            if (!myModel.GetConnectionStatus())
            {
                MessageBox.Show("Connection to Tekla is lost. Power Tool Box needs to be restarted.");
                return false;
            }
            return true;
        }

        //public bool ModelOpened()
        //{
        //    try
        //    {
        //        TSM.Model myModel = new TSM.Model();

        //        if (myModel.GetInfo().ModelName == null)
        //        {
        //            MessageBox.Show("Tekla is not opened.");
        //            return false;
        //        }
        //        else return true;
                
        //    }
        //    catch (Exception)
        //    {
        //        MessageBox.Show("Tekla is not opened!");
        //    }

        //    return false;
        //}

        //public void ModelConnectionExists(Window w)
        //{
        //    try
        //    {
        //        TSM.Model myModel = new TSM.Model();

        //        if (!myModel.GetConnectionStatus())
        //        {
        //            MessageBox.Show("Tekla is not opened.");
        //            w.Close();
        //            //Environment.Exit(0);
        //        }
        //    }
        //    catch (Exception) { }
        //}

        public void Exit(Window w)
        {
            //Environment.Exit(0);
            w.Close();
        }

        /// <summary>
        /// Add Model Selection to the List
        /// </summary>
        /// <param name="objects"></param>
        /// <returns></returns>
        public List<TSM.Object> AddModelSelectionToList(TSM.ModelObjectEnumerator objects)
        {
            List<TSM.Object> objList = new List<TSM.Object>();
            try
            {
                foreach (TSM.Object o in objects)
                {
                    //print(o.ToString());

                    if (o.ToString() == "Tekla.Structures.Model.Beam")
                    {
                        TSM.Part p = o as TSM.Part;
                        objList.Add(o);
                    }
                    if (o.ToString() == "Tekla.Structures.Model.ContourPlate")
                    {
                        TSM.ContourPlate p = o as TSM.ContourPlate;
                        objList.Add(o);
                    }
                    else if (o.ToString() == "Tekla.Structures.Model.PolyBeam")
                    {
                        TSM.PolyBeam p = o as TSM.PolyBeam;
                        objList.Add(o);
                    }
                    else if (o.ToString() == "Tekla.Structures.Model.BoltArray")
                    {
                        TSM.BoltGroup p = o as TSM.BoltGroup;
                        objList.Add(o);
                    }
                }
            }
            catch (Exception ex) { print(ex.Message); TSO.Operation.DisplayPrompt(ex.Message.ToString()); }

            return objList;
        }


        public void print(dynamic p)
        {
            p.ToString();
            Console.WriteLine(p);
        }
    }
}
