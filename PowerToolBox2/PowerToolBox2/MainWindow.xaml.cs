﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
using System.Windows;
//using System.Windows.Controls;
//using System.Windows.Data;
//using System.Windows.Documents;
//using System.Windows.Input;
//using System.Windows.Media;
//using System.Windows.Media.Imaging;
//using System.Windows.Navigation;
//using System.Windows.Shapes;

////
//using System.Collections;

//using TS = Tekla.Structures;
//using TSM = Tekla.Structures.Model;
//using TSD = Tekla.Structures.Drawing;
//using TSMUI = Tekla.Structures.Model.UI;
//using TMB = TeklaMacroBuilder;
//using TSMI = Tekla.Structures.ModelInternal;

//using System.Runtime.InteropServices;

//using System.Runtime.Remoting;
//using Tekla.Structures.Internal;

namespace PowerToolBox2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        CodeBase CB = new CodeBase();

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            if (!CB.ModelConnectionExists()) { return; }
            //CB.GetModelConnectionStatus();
            //if (!CB.ModelOpened()) { return; }

            WeldIntruderSearch wis = new WeldIntruderSearch();
            wis.Show();
        }
    }
}
