﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

//
using System.Collections;

//using Tekla.Structures.Model;
using TS = Tekla.Structures;
using TSM = Tekla.Structures.Model;
using TSD = Tekla.Structures.Drawing;
using TSMUI = Tekla.Structures.Model.UI;
using TMB = TeklaMacroBuilder;
using TSMI = Tekla.Structures.ModelInternal;

using System.Runtime.InteropServices;

using System.Runtime.Remoting;
using Tekla.Structures.Internal;

namespace PowerToolBox2
{
    /// <summary>
    /// Interaction logic for WeldIntruderSearch.xaml
    /// </summary>
    public partial class WeldIntruderSearch : Window
    {
        public WeldIntruderSearch()
        {
            InitializeComponent();
            
            this.Title = Variables.caption.ToString();         
        }

        CodeBase CB = new CodeBase();

        public ArrayList prtArrayListA = new ArrayList();
        public ArrayList prtArrayListB = new ArrayList();
        public ArrayList intruderList = new ArrayList();
        public ArrayList duplicateList = new ArrayList();
        public ArrayList idListA = new ArrayList();
        public ArrayList idListB = new ArrayList();
        public PartsToBeCompared check = new PartsToBeCompared();

        private void CheckGroups(ArrayList groupA, ArrayList groupB)
        {
            try
            {
                check = new PartsToBeCompared();

                if (prtArrayListA.Count == 0 || prtArrayListB.Count == 0)
                {
                    TSM.Operations.Operation.DisplayPrompt(Variables.promptGroupsNotAssigned);
                    return;
                }
                if (check.DuplicatesExist(prtArrayListA, prtArrayListB))
                {
                    duplicateList = check.GetDuplicates(prtArrayListA, prtArrayListB);
                    check.SelectGroup(duplicateList);
                    TSM.Operations.Operation.DisplayPrompt(Variables.promptDuplicates);
                    return;
                }

                if (check.IntruderWeldExist(groupA, groupB))
                {
                    check.SelectIntruders();
                    TSM.Operations.Operation.DisplayPrompt(Variables.promptIntruder);
                    return;
                }
                else
                {
                    TSM.Operations.Operation.DisplayPrompt(Variables.promptNoIntruder);
                }

                this.Activate();        // Bring window to front
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            check.SelectIntruders();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            // select A
            check.SelectGroup(prtArrayListA);
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            // select B
            check.SelectGroup(prtArrayListB);
        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {
            // add selected to A
            prtArrayListA = check.AddSelectedToTheList(prtArrayListA);
        }

        private void button6_Click(object sender, RoutedEventArgs e)
        {
            // remove selected from A
            prtArrayListA = check.RemoveSelectedFromTheList(prtArrayListA);
        }

        private void button5_Click_1(object sender, RoutedEventArgs e)
        {
            // add select to B
            prtArrayListB = check.AddSelectedToTheList(prtArrayListB);
        }

        private void button7_Click(object sender, RoutedEventArgs e)
        {
            // remove selected from B
            prtArrayListB = check.RemoveSelectedFromTheList(prtArrayListB);
        }

        private void button8_Click(object sender, RoutedEventArgs e)
        {
            // compare
            //idListA = check.CreateIDlist(prtArrayListA);
            idListB = check.CreateIDlist(prtArrayListB);
            CheckGroups(prtArrayListA, idListB);
        }

        private void button9_Click(object sender, RoutedEventArgs e)
        {
            // select duplicates
            duplicateList = check.GetDuplicates(prtArrayListA, prtArrayListB);
            check.SelectGroup(duplicateList);
        }
    }





    public class PartsToBeCompared
    {
        //#region needed for showOnlySelected method, to simulate shift press event
        //[DllImport("user32.dll", EntryPoint = "keybd_event", CharSet = CharSet.Auto, ExactSpelling = true)]
        //public static extern void keybd_event(byte vk, byte scan, int flags, int extrainfo);
        //public const byte KEYBDEVENTF_SHIFTVIRTUAL = 0x10;
        //public const byte KEYBDEVENTF_SHIFTSCANCODE = 0x2A;
        //public const int KEYBDEVENTF_KEYDOWN = 0;
        //public const int KEYBDEVENTF_KEYUP = 2;
        //#endregion

        //public List<TSM.Part> partListA { get; private set; }
        //public List<TSM.Part> partListB { get; private set; }
        public List<string> listA { get; private set; }
        public List<string> listB { get; private set; }
        //public ArrayList intruderList { get; private set; }
        //public ArrayList intruderList;
        //public ArrayList prtArrayListA = new ArrayList();
        //public ArrayList prtArrayListB = new ArrayList();
        public ArrayList intruderList = new ArrayList();
        public ArrayList duplicateList = new ArrayList();
        //public ArrayList idListA = new ArrayList();
        //public ArrayList idListB = new ArrayList();
        //public PartsToBeCompared check = new PartsToBeCompared();


        public ArrayList GetSelectedObjects()
        {
            ArrayList newList = new ArrayList();
            ArrayList list = new ArrayList();

            // Get Selected Objects:
            TSMUI.ModelObjectSelector mos = new TSMUI.ModelObjectSelector();
            TSM.ModelObjectEnumerator moe = mos.GetSelectedObjects();

            newList = CreateListOfParts(moe);

            list.AddRange(newList);

            return list;
        }

        public ArrayList AddSelectedToTheList(ArrayList existingList)
        {
            ArrayList list = new ArrayList();

            // Get Selected Objects:
            TSMUI.ModelObjectSelector mos = new TSMUI.ModelObjectSelector();
            TSM.ModelObjectEnumerator moe = mos.GetSelectedObjects();

            // Add new parts to the list:
            list = CreateListOfParts(moe);

            list.AddRange(existingList);

            return list;
        }

        public ArrayList RemoveSelectedFromTheList(ArrayList existigList)
        {
            ArrayList list = new ArrayList();

            // Get Selected Objects:
            TSMUI.ModelObjectSelector mos = new TSMUI.ModelObjectSelector();
            TSM.ModelObjectEnumerator moe = mos.GetSelectedObjects();

            // Add new parts to the list:
            list = CreateListOfParts(moe);

            // Remove parts from the list:
            list = RemovePartsFromListBonListA(existigList, list);

            return list;
        }

        public ArrayList RemovePartsFromListBonListA(ArrayList listA, ArrayList listB)
        {
            ArrayList listA_clone = new ArrayList();
            listA_clone = listA;
            listA_clone.ToArray();

            int i = 0;
            int n = 0;

            ArrayList itemsToRemove = new ArrayList();

            foreach (TSM.Part partA in listA.ToArray())
            {
                foreach (TSM.Part partB in listB.ToArray())
                {
                    n++;
                    if (partA.Identifier.ID == partB.Identifier.ID)
                    {
                        // Add items indexs to be removed on the list:
                        itemsToRemove.Add(i);
                    }
                }
                i++;
            }

            // Reorder itemsToRemove array, so we start removing from the back of the list:
            itemsToRemove.Sort();
            itemsToRemove.Reverse();

            // Finally remove the parts from the list:
            foreach (int r in itemsToRemove)
            {
                listA_clone.RemoveAt(r);
            }

            return listA_clone;
        }

        public ArrayList CreateIDlist(ArrayList partList)
        {
            ArrayList idList = new ArrayList();
            foreach (TSM.Part p in partList)
            {
                idList.Add(p.Identifier.ID.ToString());
            }

            return idList;
        }

        public bool DuplicatesExist(ArrayList listA, ArrayList listB)
        {
            ArrayList duplicates = new ArrayList();
            foreach (TSM.Part pA in listA)
            {
                foreach (TSM.Part pB in listB)
                {
                    if (pA.Identifier.ToString() == pB.Identifier.ToString())
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public ArrayList GetDuplicates(ArrayList listA, ArrayList listB)
        {
            ArrayList duplicates = new ArrayList();
            foreach (TSM.Part pA in listA)
            {
                foreach (TSM.Part pB in listB)
                {
                    if (pA.Identifier.ToString() == pB.Identifier.ToString())
                    {
                        duplicates.Add(pB);
                    }
                }
            }
            return duplicates;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listA">part list A</param>
        /// <param name="listB">ID list B</param>
        /// <returns></returns>
        public bool IntruderWeldExist(ArrayList listA, ArrayList listB)
        {
            bool intrudersExist = false;
            foreach (TSM.Part pA in listA)
            {
                TSM.ModelObjectEnumerator welds = pA.GetWelds();
                while (welds.MoveNext())
                {
                    TSM.Weld w = welds.Current as TSM.Weld;
                    if (w is TSM.Weld)
                    {
                        string AidMain = w.MainObject.Identifier.ToString();
                        string AidSec = w.SecondaryObject.Identifier.ToString();

                        if (listB.Contains(AidMain) || listB.Contains(AidSec))
                        {
                            intruderList.Add(w);
                            intrudersExist = true;
                        }
                    }
                }
            }
            return intrudersExist;
        }

        //public bool IntruderWeldExist()
        //{
        //    bool intrudersExist = false;
        //    foreach (TSM.Part pA in partListA)
        //    {
        //        TSM.ModelObjectEnumerator welds = pA.GetWelds();
        //        while (welds.MoveNext())
        //        {
        //            TSM.Weld w = welds.Current as TSM.Weld;
        //            if (w is TSM.Weld)
        //            {
        //                string idMain = w.MainObject.Identifier.ToString();
        //                string idSec = w.SecondaryObject.Identifier.ToString();
        //                if (listB.Contains(idMain) || listB.Contains(idSec))
        //                {
        //                    intruderList.Add(w);
        //                    intrudersExist = true;
        //                }
        //            }
        //        }
        //    }
        //    return intrudersExist;
        //}

        public void SelectIntruders()
        {
            TSM.UI.ModelObjectSelector mos = new Tekla.Structures.Model.UI.ModelObjectSelector();
            mos.Select(intruderList);
        }

        public void SelectGroup(ArrayList list)
        {
            TSM.UI.ModelObjectSelector mos = new Tekla.Structures.Model.UI.ModelObjectSelector();
            mos.Select(list);
        }

        public ArrayList CreateListOfParts(TSM.ModelObjectEnumerator moe)
        {
            ArrayList prtArrayList = new ArrayList();
            Tekla.Structures.Model.UI.ModelObjectSelector modelSelector = new Tekla.Structures.Model.UI.ModelObjectSelector();
            modelSelector.Select(prtArrayList);

            while (moe.MoveNext())
            {
                TSM.Part part = moe.Current as TSM.Part;
                if (part != null)
                {
                    prtArrayList.Add(part);
                }
            }

            return prtArrayList;
        }

        //public void HideParts(ArrayList prtList)
        //{
        //    ArrayList SelectArrayList = new ArrayList();
        //    Tekla.Structures.Model.UI.ModelObjectSelector modelSelector = new Tekla.Structures.Model.UI.ModelObjectSelector();
        //    modelSelector.Select(SelectArrayList);

        //    //foreach (TSM.Part prt in prtList)
        //    //{
        //    //    if (prt != null)
        //    //    {
        //    //        SelectArrayList.Add(prt);
        //    //    }
        //    //}
        //    modelSelector.Select(prtList);

        //    // to show only the selected items we simulate key presses
        //    // shift down
        //    keybd_event(KEYBDEVENTF_SHIFTVIRTUAL, KEYBDEVENTF_SHIFTSCANCODE, KEYBDEVENTF_KEYDOWN, 0);

        //    // Macrobuilder.dll (compiled with the correct Tekla version) is needed for this to work.
        //    TMB.MacroBuilder macroBuilder = new TMB.MacroBuilder();
        //    macroBuilder.Callback("acmdHideSelectedObjects", "", "View_01 window_1");
        //    macroBuilder.Run();

        //    // shift up
        //    keybd_event(KEYBDEVENTF_SHIFTVIRTUAL, KEYBDEVENTF_SHIFTSCANCODE, KEYBDEVENTF_KEYUP, 0);
        //}

        //public void RedrawWindow()
        //{

        //    // Macrobuilder.dll (compiled with the correct Tekla version) is needed for this to work.
        //    TMB.MacroBuilder macroBuilder = new TMB.MacroBuilder();
        //    macroBuilder.Callback("acmd_redraw_selected_view", "", "View_01 window_1");
        //    macroBuilder.Run();
        //}

        //public ArrayList MergeArrays(prtArrayListA, tmp_prtArrayListA)
        //{

        //}
    }

    public class Variables
    {
        public static string caption = "Weld Intruder Tool 2017i v0.3";
        //public static string version = "0.2";
        //public static string promptA = "Pick parts for group A. Middle button to confirm selection";
        //public static string promptB = "Pick parts for group B. Middle button to confirm selection";
        //public static string promptNothing = String.Format("Nothing selected, {0} will exit.", caption);
        public static string promptDuplicates = String.Format("There are duplicates amongst groups A and B. {0}", caption);
        public static string promptIntruder = String.Format("Intruder welds between groups A and B are selected. {0}", caption);
        public static string promptNoIntruder = String.Format("There are no intruder welds found. {0}", caption);
        public static string promptGroupsNotAssigned = String.Format("No parts assigned to Group A and group B. {0}", caption);
    }
}
